# dyn-iptv [![coverage report](https://gitlab.com/rubdos/dyn-iptv/badges/master/coverage.svg)](https://gitlab.com/rubdos/dyn-iptv/commits/master) [![pipeline status](https://gitlab.com/rubdos/dyn-iptv/badges/master/pipeline.svg)](https://gitlab.com/rubdos/dyn-iptv/commits/master)

Proxy for complex IPTV services.
Useful for exporting token-based IPTV or streaming services (web streamed concerts, for instance) to TVHeadend.

Usage:

```
cargo build --release
scp target/release/dyn-iptv TARGET_SYSTEM
# make config in /etc/dyn-iptv/channels.toml
# as example, see example-config directory
# run service, for example using the included systed service file
```

The example contains the live streams for the Rock Werchter festival.
Other ProximusTV based streams should be easy as a config line.

dyn-iptv
Copyright (C) 2018  Ruben De Smet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

// #![feature(custom_attribute)]
#![feature(never_type)]
#![feature(proc_macro)]
#![feature(generators)]
#![feature(proc_macro_non_items)]
#![feature(proc_macro_gen)]

#[macro_use]
extern crate clap;
extern crate config;
extern crate env_logger;
extern crate failure;
#[macro_use]
extern crate failure_derive;
extern crate futures_await as futures;
extern crate http;
extern crate hyper;
extern crate hyper_tls;
#[macro_use]
extern crate log;
extern crate select;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
extern crate tokio;

#[macro_use]
mod util;
mod settings;
mod provider;

use std::path::Path;

use futures::future::ok;
use futures::prelude::*;
use futures::prelude::await;
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use hyper::rt::Future;
use hyper::service::Service;

static TEXT: &str = "Hello, World!";

#[derive(Clone)]
struct M3u8Service {
    channels: settings::Channels,
}

impl M3u8Service {
    fn generate_m3u8(&self, ch_name: String, chan: &settings::Channel) -> Box<Future<Item=Response<Body>, Error=!> + Send> {
        let mut _500 = {
            let mut r = Response::new(Body::empty());
            *r.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
            r
        };

        match provider::from_channel(chan) {
            Ok(provider) => Box::new(async_block!{
                let config = await!(provider.generate_url())
                    .expect("Provider url");
                let provider::StreamConfig {
                    name, url, poster: _,
                } = config;

                // let m3u = format!(
                //     include_str!("single_template.m3u8"),
                //     name.unwrap_or(ch_name.to_owned()),
                //     url
                // );
                let response = Response::builder()
                    .status(302)
                    // .header("Access-Control-Allow-Origin", "*")
                    // .header("Access-Control-Allow-Headers", "Content-Type")
                    // .header("Content-Type", "application/x-mpegURL")
                    .header("Location", &url as &str)
                    // .body(Body::from(m3u))
                    .body(Body::empty())
                    .unwrap();
                Ok(response)
            }),
            Err(e) => {
                *_500.body_mut() = Body::from(format!("{}", e));
                Box::new(ok(_500))
            },
        }
    }
}

impl Service for M3u8Service {
    type ReqBody = Body;
    type ResBody = Body;
    type Error = !;
    type Future = Box<Future<Item=Response<Self::ResBody>, Error=Self::Error> + Send>;

    fn call(&mut self, req: Request<Body>) -> Self::Future {
        let _404 = {
            let mut r = Response::new(Body::empty());
            *r.status_mut() = StatusCode::NOT_FOUND;
            Box::new(ok(r))
        };

        match (req.method(), req.uri().path()) {
            (&Method::GET, "/") => Box::new(ok(Response::new(Body::from(TEXT)))),
            (&Method::GET, path) => {
                let mut path = &path[1..];
                if &path[path.len()-5..] == ".m3u8" {
                    path = &path[..path.len()-5];
                }
                info!("path == {}", path);

                if let Some(chan) = self.channels.get(path) {
                    info!("accessing channel `{}'", path);
                    info!(" type = {}", chan.source);
                    self.generate_m3u8(path.to_owned(), chan)
                } else {
                    _404
                }
            }
            _ => _404,
        }
    }
}

fn main() {
    env_logger::init();

    let matches = clap_app!(dyn_iptv =>
            (version: "0.1.0")
	        (author: "Ruben De Smet")
	        (about: "iptv redirection")
	        (@arg CONFIG: -c --config +takes_value "Sets the configuration file directory")
	        );
    let matches = matches.get_matches();

    let config_dir = matches.value_of("CONFIG")
        .map(Path::new);

    if let Some(config_dir) = config_dir {
        debug!("Using config directory `{}'", config_dir.display());
    }

    let channels = settings::channels(config_dir.into())
        .expect("Invalid configuration");

    let service = M3u8Service {
        channels,
    };

    let addr = ([127, 0, 0, 1], 3001).into();

    let new_svc = move || {
        ok::<M3u8Service, !>(service.clone())
    };

    let server = Server::bind(&addr)
        .serve(new_svc)
        .map_err(|e| eprintln!("server error: {}", e));

    hyper::rt::run(server);
}

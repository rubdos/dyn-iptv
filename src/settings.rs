use std::path::Path;
use std::collections::HashMap;

use config::{ConfigError, Config, File};

#[derive(Clone, Debug, Deserialize)]
pub struct Channel {
    pub source: String,
    pub url: String,
}

pub type Channels = HashMap<String, Channel>;

pub fn channels(extra_path: Option<&Path>) -> Result<Channels, ConfigError> {
    let mut s = Config::new();

    let etc_path = Path::new("/etc/dyn-iptv/channels.toml");

    if etc_path.is_file() {
        s.merge(File::with_name("/etc/dyn-iptv/channels.toml"))?;
    } else {
        warn!("`{}' is not a file. Not using.",
              etc_path.display());
    }

    if let Some(extra_path) = extra_path {
        let channels = extra_path.join("channels.toml");
        s.merge(File::from(channels))?;
    }

    trace!("merged config: {:?}", s);

    let channels: HashMap<_, _> = s.try_into()?;
    Ok(channels)
}

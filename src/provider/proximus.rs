use std;
use futures::future::ok;
use futures::prelude::{async_block, await};
use hyper::{self, Body, Client, StatusCode, Uri};
use hyper::client::HttpConnector;
use hyper::header::LOCATION;
use hyper::rt::{Future, Stream};
use hyper_tls::HttpsConnector;

use select::document::Document;
use select::predicate::Attr;

use ::settings::Channel;
use ::provider::{Provider, ProviderError};

/// Currently, Proximus is implemented as a scraper from the video frame HTML page. It should
/// however be possible to "crack" the way the token gets generated. The token is an md5 hash,
/// encoded in hex, from some combination of (possibly) the source IP address, the expiry
/// timestamp, and the filename.
///
/// Most services that serve HLS with such a token and expiry date use a base 64 encoded md5sum, so
/// it should not be very difficult to find which one it is.
pub struct Proximus {
    url: Uri,
}

#[derive(Debug, Deserialize)]
struct Source {
    hls: String, // This is always owned because it's parsed
    poster: String,
}

#[derive(Debug, Deserialize)]
struct StreamConfig<S> {
    name: S,
    source: Source,
}

impl Proximus {
    pub fn new(channel: &Channel) -> Result<Proximus, ProviderError> {
        Ok(Proximus {
            url: channel.url.clone().parse()?,
        })
    }

    fn get_redirect(client: Client<HttpsConnector<HttpConnector>>, hls: String)
        -> impl Future<Item=String, Error=ProviderError>
    {
        async_block! {
            let res = await!(client.get(hls.parse()?))?;

            debug!("Response: {}", res.status());
            debug!("Headers: {:?}", res.headers());

            if res.status() != StatusCode::FOUND {
                error!("Expected status FOUND, got {}", res.status());
                return Err(ProviderError::NotRedirected)
            }

            Ok(res.headers()[LOCATION].to_str()?.to_owned())
        }
    }

    fn fetch_html(&self, client: &mut Client<HttpsConnector<HttpConnector>>)
            -> impl Future<Item=hyper::Body, Error=ProviderError>
    {
        client
            .get(self.url.clone())
            .map(move |res| {
                debug!("Response: {}", res.status());
                debug!("Headers: {:?}", res.headers());
                res.into_body()
            })
            .map_err(|err| {
                error!("Error response: {}", err);
                err
            })
            .map_err(Into::into)
    }

    fn parse_html<'s, S: Future<Item=Body, Error=ProviderError> + 's>(html: S)
        -> impl Future<Item=StreamConfig<String>, Error=ProviderError> +'s
    {
        async_block! {
            let html = await!(html)?;
            let html = await!(html.concat2())?;
            let document = Document::from(std::str::from_utf8(&html)?);

            let player = document.find(Attr("id", "player"))
                .next()
                .ok_or(ProviderError::NoPlayerError{cause: "No element with id player"})?;
            let json = player.attr("data-config")
                .ok_or(ProviderError::NoPlayerError{cause: "No data-config attribute"})?;

            let config: StreamConfig<_> = ::serde_json::from_str(json)
                .map_err(|e| {
                    error!("Cannot deserialise stream config ({})", e);
                    ProviderError::NoPlayerError{cause: "Invalid stream configuration"}
                })?;

            Ok(config)
        }
    }
}

impl Provider for Proximus {
    fn generate_url(&self) -> Box<Future<Item=::provider::StreamConfig, Error=ProviderError> + Send> {
        let https = HttpsConnector::new(4).unwrap();
        let mut client = Client::builder()
            .keep_alive(true)
            .build::<_, hyper::Body>(https);

        let html = self.fetch_html(&mut client);
        Box::new(async_block! {
            let StreamConfig {
                name,
                source: Source {
                    hls, poster,
                },
            } = await!(Self::parse_html(html))?;

            debug!("HLS url: `{}'", hls);
            let url = await!(Self::get_redirect(client, hls))?;

            Ok(::provider::StreamConfig {
                name: none_if_empty!(name),
                url,
                poster: none_if_empty!(poster),
            })
        })
    }
}

#[test]
fn parse_html() {
    ::env_logger::init();

    let html = include_str!("test-data/festival.html");
    let config = Proximus::parse_html(ok(Body::from(html)))
        .wait()
        .unwrap();

    assert_eq!(config.source.hls,
               "https://live.streaming.proximustv.be/live_festivals_hls_dyn/dvr-festivals_stage1/index.m3u8?token=866ccd5191afdf45f7f9a43ccb94b686&expires=1530884909");
}

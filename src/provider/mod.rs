use std;
use http;
use hyper;

use hyper::header::ToStrError;

use futures::prelude::*;
use ::settings::Channel;

mod proximus;

#[derive(Debug, Fail)]
pub enum ProviderError {
    #[fail(display = "invalid uri")]
    InvalidUri(#[cause] http::uri::InvalidUri),
    #[fail(display = "request error")]
    RequestError(#[cause] hyper::Error),
    #[fail(display = "request parse error")]
    ParseError(#[cause] std::str::Utf8Error),
    #[fail(display = "request parse error: no player in HTML ({})", cause)]
    NoPlayerError{
        cause: &'static str,
    },
    #[fail(display = "invalid provider type `{}'", provider)]
    NoSuchProvider{provider: String},
    #[fail(display = "Invalid header")]
    InvalidHeader,
    #[fail(display = "not redirected")]
    NotRedirected,
}

pub struct StreamConfig {
    pub name: Option<String>,
    pub url: String,
    pub poster: Option<String>,
}

impl From<ToStrError> for ProviderError {
    fn from(_: ToStrError) -> ProviderError {
        ProviderError::InvalidHeader
    }
}

impl From<std::str::Utf8Error> for ProviderError {
    fn from(iu: std::str::Utf8Error) -> ProviderError {
        ProviderError::ParseError(iu)
    }
}

impl From<http::uri::InvalidUri> for ProviderError {
    fn from(iu: http::uri::InvalidUri) -> ProviderError {
        ProviderError::InvalidUri(iu)
    }
}

impl From<hyper::Error> for ProviderError {
    fn from(err: hyper::Error) -> ProviderError {
        ProviderError::RequestError(err)
    }
}

pub fn from_channel(channel: &Channel) -> Result<Box<Provider + Send>, ProviderError> {
    match channel.source.as_str() {
        "proximustv" => Ok(Box::new(proximus::Proximus::new(channel)?)),
        _ => Err(ProviderError::NoSuchProvider{provider: channel.source.clone()}),
    }
}

pub trait Provider {
    fn generate_url(&self) -> Box<Future<Item=StreamConfig, Error=ProviderError> + Send>;
}

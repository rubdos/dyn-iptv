#[macro_export]
macro_rules! none_if_empty {
    ($v:expr) => {{
        let temp = $v;
        if temp.is_empty() {
            None
        } else {
            Some(temp)
        }
    }}
}
